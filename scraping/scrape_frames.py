"""
Script to scrape frame data from Tool assisted's google spreadsheet of MVCI frame data
David Martin
10/21/2017
"""
import httplib2
import os
import sqlite3

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'  # credentials
APPLICATION_NAME = 'Google Sheets API Python Quickstart'  # name of application in google apps, needed to use the api
SPREADSHEETID = '1-8bUWUvF_479T6sJYlvZ5Xc0aSq005l9rKmmNoaABNc'  # spreadsheet id from spreadsheet url

# maps characters from spreadsheet to their name in the sqlite db
char_dict = {
    'cpt marvel': 'Captain Marvel',
    'dante': 'Dante',
    'doc strange': 'Doctor Strange',
    'dormammu': 'Dormammu',
    'firebrand': 'Firebrand',
    'ghost rider': 'Ghost Rider',
    'hulk': 'Hulk',
    'jedah': 'Jedah',
    'monster hunter': 'Monster Hunter',
    'morrigan': 'Morrigan',
    'rocket': 'Rocket Raccoon',
    'ryu': 'Ryu',
    'sigma': 'Sigma',
    'spencer': 'Spencer',
    'spider-man': 'Spider-Man',
    'thanos': 'Thanos',
    'thor': 'Thor',
    'ultron': 'Ultron',
    'zero': 'Zero',
    'arthur': 'Arthur',
    'black panther': 'Black Panther',
    'cpt america': 'Captain America',
    'chun': 'Chun-Li',
    'chris': 'Chris Redfield',
    'frank': 'Frank West',
    'gamora': 'Gamora',
    'haggar': 'Mike Haggar',
    'hawkeye': 'Hawkeye',
    'iron man': 'Iron Man',
    'mega man x': 'Mega Man X',
    'nemesis': 'Nemesis',
    'nova': 'Nova',
    'strider': 'Strider',
    'black widow': 'Black Widow',
    'venom': 'Venom',
    'winter soldier': 'Winter Soldier',
}

def get_credentials():
    """ Gets valid user credentials from storage. """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        print('Storing credentials to ' + credential_path)
    return credentials


def get_value(arr, index):
    """ get value from array, returns empty string if index doesn't exist """
    try:
        return arr[index]
    except IndexError:
        return None

def get_int(arr, index):
    """ get int value from array, returns None if index doesn't exist or 0 if string can't convert """
    try:
        return int(arr[index])
    except IndexError:
        return None
    except ValueError:
        return None

def main():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?''version=v4')
    service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)

    sheets = service.spreadsheets().get(spreadsheetId=SPREADSHEETID).execute()['sheets']

    count = 0

    for sheet in sheets:
        sheet_name = sheet['properties']['title']
        name = char_dict.get(sheet_name.lower())
        # print('sheet name: %s, name: %s' % (sheet_name, name))
        if name:
            print(str(count) + ') ' + name)
            count += 1
            rangeName = sheet_name + '!B2:K'
            data = service.spreadsheets().values().get(spreadsheetId=SPREADSHEETID, range=rangeName).execute()
            frame_values = data.get('values', [])

            conn = sqlite3.connect('db.sqlite3')
            c = conn.cursor()
            c.execute("SELECT id FROM frames_character WHERE name = ?", (name,))
            char_id = c.fetchone()[0]

            for row in frame_values:
                if get_value(row, 1):
                    values = (get_value(row, 0), get_value(row, 1), get_int(row, 2), get_value(row, 3), get_int(row, 4),
                              get_int(row, 5), get_int(row, 6), get_int(row, 7), get_int(row, 8), get_int(row, 9), char_id)
                    c.execute("INSERT INTO frames_move (name, command, startup, active, recovery, total, damage, "
                              "block_advantage, hit_advantage, counter_hit_advantage, character_id)"
                              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values)
            conn.commit();
            conn.close();
        else:
            print('%s not found' % sheet_name.lower())


if __name__ == '__main__':
    main()
