from django.contrib import admin

from frames.models import Character, Move

admin.site.register(Character)
admin.site.register(Move)

