from frames.models import Character, Move
from frames.serializers import CharacterSerializer, MoveSerializer
from rest_framework import mixins
from rest_framework import generics


class CharacterList(generics.ListAPIView):
    queryset = Character.objects.all()
    serializer_class = CharacterSerializer


class MoveList(generics.ListAPIView):
    queryset = Move.objects.all()
    serializer_class = MoveSerializer


class CharacterMoveList(generics.ListAPIView):
    serializer_class = MoveSerializer

    def get_queryset(self):
        """
        Return all moves belonging to a certain character
        """
        return Move.objects.filter(character=self.kwargs['character_id'])

