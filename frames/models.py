from django.db import models

class Character(models.Model):

    name = models.CharField(max_length=100)
    health = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Move(models.Model):

    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True, blank=True)
    command = models.CharField(max_length=30)
    startup = models.IntegerField(null=True)
    active = models.CharField(max_length=20, null=True, blank=True)
    recovery = models.IntegerField(null=True)
    total = models.IntegerField(null=True)
    hit_advantage = models.IntegerField(null=True)
    counter_hit_advantage = models.IntegerField(null=True)
    block_advantage = models.IntegerField(null=True)
    damage = models.IntegerField(null=True)
    cancellable = models.CharField(max_length=30, null=True, blank=True)
    notes = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.character.name + ' ' + self.name

