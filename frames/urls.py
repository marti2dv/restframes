from django.conf.urls import url
from frames import views

urlpatterns = [
    url(r'^characters/$', views.CharacterList.as_view()),
    url(r'^moves/$', views.MoveList.as_view()),
    url(r'^characters/(?P<character_id>[0-9]+)/moves$', views.CharacterMoveList.as_view()),
]
