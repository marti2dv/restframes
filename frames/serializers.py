from frames.models import Character, Move
from rest_framework import serializers

class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Character
        fields = ('id', 'name', 'health')


class MoveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Move
        fields = ('id', 'character', 'name', 'command', 'startup', 'active',
                  'recovery', 'total', 'hit_advantage', 'counter_hit_advantage',
                  'block_advantage', 'damage', 'cancellable', 'notes')

